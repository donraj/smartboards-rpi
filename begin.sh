echo 'begin.sh was started at '$(date) >> /home/pi/maal/log.txt


export http_proxy='http://10.10.78.22:3128'
export https_proxy='http://10.10.78.22:3128'
export URL='http://notices.smartcampus.iitd.ac.in/testing/?id=4'
source /home/pi/maal/scripts/bash_extras.sh # This is not really right!!

# autologin script
if [ ! -f /home/pi/maal/scripts/autologin.pyc ]; then
    cecho "autologin.pyc NOT FOUND in scripts folder..." $red
else
    if ! screen -list | grep -q "login"; then
        cecho "starting autologin screen" $blue
        screen -dmS login python /home/pi/maal/scripts/autologin.pyc
    else
        cecho "Login screen already exists" $yellow
    fi
fi

# epiphany script
screen -dmS epiphany sh /home/pi/maal/scripts/manage_epiphany.sh

# master monitoring script ...
