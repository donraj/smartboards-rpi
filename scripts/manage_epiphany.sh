sleep 40
xte "mousemove 10000 10000"
alias epi_cmd='epiphany -a --profile /home/pi/.config $URL'
sh -c 'sleep 7; xte "key F11"; sleep 5; xte "key F5"' & epi_cmd
while true;
do
   echo 'checking epiphany' $blue
   case "$(pidof epiphany-browser | wc -w)" in
   0) echo "Restarting Epiphany $(date)"
      sh -c 'sleep 7; xte "key F11"; sleep 5; xte "key F5"' & epi_cmd
      ;;
   1) #all ok
      ;;
   *) echo "Removed Double Epiphany Browser $(date)"
      kill $(pidof epiphany-browser | awk ´{print $1}´)
      ;;
   esac
   wait $!
   sleep 600
done
