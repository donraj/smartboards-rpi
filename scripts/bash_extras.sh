#!/bin/bash
# color-echo.sh: Echoing text messages in color.

# Modify this script for your own purposes.
# It's easier than hand-coding color.

black='\033[90m'
red='\033[91m'
green='\033[92m'
yellow='\033[93m'
blue='\033[94m'
magenta='\033[95m'
cyan='\033[96m'
white='\033[97m'

alias Reset="tput sgr0"      #  Reset text attributes to normal
                             #+ without clearing screen.

cecho ()
{
  # Color-echo.
  # Argument $1 = message
  # Argument $2 = color
  local default_msg="No message passed."
  message=${1:-$default_msg}   # Defaults to default message.
  color=${2:-$white}           # Defaults to white, if not specified.
  echo -e "$color$message"
  tput sgr0 # reset to normal text settings
  return
}

# Now, let's try it out.
# ----------------------------------------------------
test_cecho() {
  cecho "Feeling blue..." $blue
  cecho "Magenta looks more like purple." $magenta
  cecho "Green with envy." $green
  cecho "Seeing red?" $red
  cecho "Cyan, more familiarly known as aqua." $cyan
  cecho "No color passed (defaults to black)."
         # Missing $color argument.
  cecho "\"Empty\" color passed (defaults to black)." ""
         # Empty $color argument.
  cecho
         # Missing $message and $color arguments.
  cecho "" ""
       # Empty $message and $color arguments.
}
# ----------------------------------------------------


# Exercises:
# ---------
# 1) Add the "bold" attribute to the 'cecho ()' function.
# 2) Add options for colored backgrounds.
